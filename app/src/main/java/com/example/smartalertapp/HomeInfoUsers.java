package com.example.smartalertapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class HomeInfoUsers extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference uRef;
    int LED;
    private List<String> InfoStatus;
    ListView ListUserInfo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu_second_home);
        ListUserInfo = (ListView) findViewById(R.id.info_list);

        uRef = FirebaseDatabase.getInstance().getReference();


        FirebaseUser user = mAuth.getInstance().getCurrentUser();
        uRef.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                GenericTypeIndicator<List<String>> t = new GenericTypeIndicator<List<String>>() {};
                InfoStatus = dataSnapshot.child("Info").getValue(t);

                updateUI();
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void  updateUI() {
        ArrayAdapter <String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, InfoStatus);

        ListUserInfo.setAdapter(adapter);
    }
}
