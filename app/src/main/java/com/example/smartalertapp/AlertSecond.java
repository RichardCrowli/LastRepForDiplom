package com.example.smartalertapp;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Random;


public class AlertSecond extends AppCompatActivity implements AdapterView.OnItemSelectedListener, OnOptionsItemSelected {

    AlarmManager alarm_Manager;
    TimePicker alarm_timePicker;
    TextView update_text;
    Context context;
    PendingIntent pending_Intent;


    @Override
    protected void onCreate(Bundle saveInstanceState) {

        super.onCreate(saveInstanceState);
        setContentView(R.layout.second_alert);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        this.context = this;

        alarm_Manager = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarm_timePicker = (TimePicker) findViewById(R.id.alarmTimePicker);

        update_text = (TextView) findViewById(R.id.alarmText);

        final Calendar calendar = Calendar.getInstance();
        final Intent my_intent = new Intent(this.context, AlarmReceiver_Second.class);

        Spinner spinner = (Spinner) findViewById(R.id.soundspinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sound_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        Button alarm_on = (Button) findViewById(R.id.start_alarm);

        alarm_on.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                calendar.set(Calendar.HOUR_OF_DAY, alarm_timePicker.getHour());
                calendar.set(Calendar.MINUTE, alarm_timePicker.getMinute());

                int hour = alarm_timePicker.getHour();
                int minute = alarm_timePicker.getMinute();

                String hour_string = String.valueOf(hour);
                String minute_string = String.valueOf(minute);

                if(hour > 12) {
                    hour_string = String.valueOf(hour - 12);
                }
                if(minute < 10) {
                    minute_string = "0" + String.valueOf(minute);
                }

                setAlarmText("Будильник поставлен на " + hour_string + ":" + minute_string);

                my_intent.putExtra("extra", "Будильник включён");
                pending_Intent = PendingIntent.getBroadcast(AlertSecond.this, 0 ,
                        my_intent, PendingIntent.FLAG_UPDATE_CURRENT);

                alarm_Manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                        pending_Intent);
            }




        });

        Button alarm_off = (Button) findViewById(R.id.stop_alarm);
        alarm_off.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setAlarmText("Будильник выключен");
                alarm_Manager.cancel(pending_Intent);
                my_intent.putExtra("extra","Будильник выключен");
                sendBroadcast(my_intent);
            }
        });
    }


    public void setAlarmText(String alarmText) {
        update_text.setText(alarmText);
    }

    @Override

    public boolean OnOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback

    }


    /**/


}
