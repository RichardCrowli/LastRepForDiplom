package com.example.smartalertapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MyBrRAll extends BroadcastReceiver {

    Ringtone r;

    @Override
    public void onReceive(Context context, Intent intent) {
        Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(2000);


        Notification noti = new Notification.Builder(context)
                .setContentTitle("Alarm is on")
                .setContentText("You had set up the alarm")
                .setSmallIcon(R.mipmap.ic_launcher).build();

        NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        noti.flags|=Notification.FLAG_AUTO_CANCEL;
        manager.notify(0,noti);


        Uri notifcation = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        r = RingtoneManager.getRingtone(context, notifcation);
        r.play();

        try {
            Thread.sleep(5000);
            r.stop();
        } catch (InterruptedException e) {
        }
    }
}
